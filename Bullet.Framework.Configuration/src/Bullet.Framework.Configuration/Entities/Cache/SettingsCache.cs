﻿using System;
using System.Collections.Generic;
using Bullet.Framework.Configuration.Entities.Parameters;
using Bullet.Framework.Configuration.Interfaces.Cache;
using Bullet.Framework.Configuration.Interfaces.Source;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Bullet.Framework.Configuration.Entities.Cache
{
    public class SettingsCache<TKey> : ISettingsProviderCache<TKey> where TKey : IEquatable<TKey>
    {
        private IList<UniversalParameter<TKey>> _configList;
        private readonly IConfigurationSettingsSource<TKey> _configurationSourceProvider;
        private const int NotFound = -1;

        private static SettingsCache<TKey> _instance;

        // Lock synchronization object
        private static object syncLock = new object();

        public SettingsCache(IConfigurationSettingsSource<TKey> source)
        {
            if (source == null) throw new ArgumentNullException();
            
            _configurationSourceProvider = source;
            _configList = new List<UniversalParameter<TKey>>();
        }
        
        public static SettingsCache<TKey> GetCache(IConfigurationSettingsSource<TKey> source)
        {
            if (_instance == null)
            {
                lock (syncLock)
                {
                    if (_instance == null)
                    {
                        _instance = new SettingsCache<TKey>(source);
                    }
                }
            }

            return _instance;
        }

        /// <summary>
        /// Get a configuration from cache based on passed generic parameter info, if doesn´t exist request it to source
        /// </summary>
        /// <param name="genericKey"></param>
        /// <param name="serializer"></param>
        /// <returns>A configuration value <see cref="TKey"/></returns>
        public TValue GetValue<TValue>(TKey genericKey, ISerializable<TValue> serializer, ILogger logger )
        {
            var index = FindConfiguration(genericKey, logger);
            if (index == NotFound)
            {
                logger.LogDebug($"Registo nao esta em cache necessario carregar do source: {JsonConvert.SerializeObject(genericKey)}");
                return LoadRecordFromSource(genericKey, serializer);
            }

            logger.LogDebug($"Registo em cache: {genericKey}");
            return (TValue)_configList[index].Value;
        }

        /// <summary>
        /// Save a new conguration in database
        /// If correctly saved in database update the record in cache
        /// </summary>
        /// <param name="key">generic parameters used to filter the configuration to be saved</param>
        /// <param name="value">generic parameters used to filter the configuration to be saved</param>
        /// <param name="serializer">generic parameters used to serialize and deserialize records</param>
        /// <returns>true if correctly saved, false otherwise</returns>
        public bool SetValue<TValue>(TKey key, TValue value, ISerializable<TValue> serializer, ILogger logger)
        {
            if (key == null) throw new ArgumentNullException("Configuração a ser gravada com valor nulo ou inválido");

            var elemToSave = serializer.SerializeRecord(value);
            var saved = _configurationSourceProvider.SaveConfigurationAsync(key, elemToSave).Result;
            if (!saved) return false;

            var universalParameters = new UniversalParameter<TKey>(key, value);

            var index = FindConfiguration(key, logger);
            if (index != NotFound)
            {
                logger.LogDebug($"Registo {value}, na cache encontra-se na posicao \'{index}\' actualizado");
                _configList[index].Value = (TValue)universalParameters.Value;
            }
            else
            {
                logger.LogDebug($"Registo: {value},  nao se encontra na cache e vai ser adicionado.");
                _configList.Add(universalParameters);
            }

            return true;
        }


        /// <summary>
        /// If a record is not present in cache request it the fist time to source
        /// </summary>
        /// <param name="genericKey">generic parameters used to filter the configuration to be request</param>
        /// <param name="serializer">generic parameter used to serializer the records load from database</param>
        /// <returns> <see cref="TKey"/> data of record request, if dont find it return default of <see cref="TKey"/> </returns>
        private TValue LoadRecordFromSource<TValue>(TKey genericKey, ISerializable<TValue> serializer)
        {
            var configuration = _configurationSourceProvider.LoadConfiguration(genericKey);
            if (configuration == null) return default(TValue);

            var config = serializer.DeserializeRecord(configuration.ToString());

            _configList.Add(new UniversalParameter<TKey>(genericKey, config));

            return config;
        }

        /// <summary>
        /// Find configuration value in list
        /// </summary>
        /// <param name="genericKey">contain info about configuration to return to user</param>
        /// <returns>index of configuration, otherwise returns a default </returns>
        private int FindConfiguration(TKey genericKey,ILogger logger)
        {
            if (_configList == null || _configList.Count == 0)
            {
                logger.LogDebug("Cache está vazia ou é nula");
                return NotFound;
            }

            for (var i = 0; i < _configList.Count; i++)
            {
                var elem = _configList[i].Key;

                if (elem.Equals(genericKey))
                {
                    return i;
                }
            }

            logger.LogDebug($"Elemento não encontrado na cache : {genericKey}");
            return NotFound;
        }
    }
}
