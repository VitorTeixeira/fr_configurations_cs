﻿using System;
using System.ComponentModel;
using Bullet.Framework.Configuration.Entities.Cache;
using Bullet.Framework.Configuration.Interfaces.Cache;
using Bullet.Framework.Configuration.Interfaces.Source;
using Microsoft.Extensions.Logging;

namespace Bullet.Framework.Configuration.Entities.Configuration
{
    public abstract class BaseConfiguration<TKey, TValue> : ISerializable<TValue> where TKey : IEquatable<TKey>
    {
        private const string InvalidSettingsProviderCache = "Fornecedor de settings inválido por favor analise a configuração enviada.";
        private const string InvalidGenericParameter = "Parâmetros de entrada são nulos ou inválidos reveja a configuração do parâmetro";
        private static ISettingsProviderCache<TKey> Cache;
        private readonly object _obj = new object();

        /// <summary>
        /// Retrieve typed value <see cref="TKey"/> from a list of configuration
        /// </summary>
        /// <param name="source"></param>
        protected BaseConfiguration(IConfigurationSettingsSource<TKey> source)
        {
            if(source == null) throw new ArgumentNullException(InvalidSettingsProviderCache);

            lock (_obj)
            {
                if (Cache != null) return;
                Cache = SettingsCache<TKey>.GetCache(source);
            }
        }

        /// <summary>
        /// Get a genericValue obtained by generic key
        /// </summary>
        /// <param name="genericKey"></param>
        /// <returns></returns>
        public virtual TValue GetValue(TKey genericKey)
        {
            if (genericKey == null) throw new ArgumentNullException(string.Format(InvalidGenericParameter));
            return Cache.GetValue(genericKey, this,GetLogger());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="genericKey"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetValue(TKey genericKey, TValue value)
        {
            if (genericKey == null) throw new ArgumentNullException(string.Format(InvalidGenericParameter));
            return Cache.SetValue(genericKey, value, this, GetLogger());
        }
        
        protected virtual string GetConfigurationName()
        {
            return nameof(TValue);
        }

        protected abstract ILogger GetLogger();

        /// <summary>
        /// Serialization of data base on data type request
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual TValue DeserializeRecord(string value)
        {
            return (TValue)TypeDescriptor.GetConverter(typeof(TValue)).ConvertFromString(value);
        }

        /// <summary>
        /// Implementation of serialization data
        /// </summary>
        /// <param name="value">any value <see cref="TKey"/> who need be serialized</param>
        /// <returns></returns>
        public virtual string SerializeRecord(TValue value)
        {
            return TypeDescriptor.GetConverter(typeof(TValue)).ConvertToString(value);
        }
    }
}