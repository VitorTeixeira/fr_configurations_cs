﻿using System;

namespace Bullet.Framework.Configuration.Entities.Parameters
{/// <summary>
 /// 
 /// </summary>
    public class DefaultConfiguration : IEquatable<DefaultConfiguration>
    {
        /// <summary>
        /// Name of configuration requested
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// User that request the configuration
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Application name related to configuration request 
        /// </summary>
        public int ApplicationKey { get; set; }

        /// <summary>
        /// Application name related to configuration request 
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Define if this configuration should be stored in cache or not
        /// </summary>
        public bool IsCached { get; set; }

        /// <summary>
        /// Stored Typed Values
        /// </summary>      
        public string Value { get; set; }


        public bool Equals(DefaultConfiguration other)
        {
            if (other == null) return false;

            return UserId == other.UserId &&
                   ConfigurationName == other.ConfigurationName &&
                   ApplicationName == other.ApplicationName;
        }

    }
}
