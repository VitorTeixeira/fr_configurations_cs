﻿using System;

namespace Bullet.Framework.Configuration.Entities.Parameters
{
    public class UniversalParameter<TKey> where TKey : IEquatable<TKey>
    {
        public TKey Key { get; set; }
        public object Value { get; set; }
        public bool IsCached { get; set; }
        public Type Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public UniversalParameter()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public UniversalParameter(TKey key) : this()
        {
            Key = key;
            IsCached = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public UniversalParameter(TKey key, object value) : this(key)
        {
            Key = key;
            Value = value;
            IsCached = true;
        }
    }
}
