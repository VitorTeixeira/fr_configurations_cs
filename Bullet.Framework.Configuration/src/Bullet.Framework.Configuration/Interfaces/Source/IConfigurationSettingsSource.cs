﻿using System;
using System.Threading.Tasks;

namespace Bullet.Framework.Configuration.Interfaces.Source
{
    /// <summary>
    /// Defines what a configuration source must implement to read configurations
    /// </summary>
    public interface IConfigurationSettingsSource<TKey> where TKey : IEquatable<TKey>
    {
        object LoadConfiguration(TKey genericParameters);

        Task<bool> SaveConfigurationAsync(TKey universalParameter, string value);
    }
}
