﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Bullet.Framework.Configuration.Interfaces.Cache
{
    public interface ISettingsProviderCache<TKey> where TKey : IEquatable<TKey>
    {
        TValue GetValue<TValue>(TKey genericKey, ISerializable<TValue> serializer, ILogger logger);

        bool SetValue<TValue>(TKey key, TValue value, ISerializable<TValue> serializer, ILogger logger);
    }
}
