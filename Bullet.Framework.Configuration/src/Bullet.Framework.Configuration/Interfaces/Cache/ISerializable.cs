﻿namespace Bullet.Framework.Configuration.Interfaces.Cache
{
    public interface ISerializable<T>
    {
        T DeserializeRecord(string value);

        string SerializeRecord(T value);
    }
}
