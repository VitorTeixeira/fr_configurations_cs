﻿using System;
using System.Globalization;
using System.ComponentModel;
using Bullet.Framework.Configuration.Entities.Configuration;
using Bullet.Framework.Configuration.Entities.Parameters;
using Bullet.Framework.Configuration.Interfaces.Source;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Bullet.Framework.Configuration.Test.Mock
{
    /// <summary>
    /// Server Side 
    /// </summary>
    public class GridConfigMock : BaseConfiguration<DefaultConfiguration, GridModelMock>
    {
        public GridConfigMock(IConfigurationSettingsSource<DefaultConfiguration> source) 
            : base(source)
        {

        }

        protected override string GetConfigurationName()
        {
            return "GridConfig";
        }

        protected override ILogger GetLogger()
        {
            return null;
        }

        public override GridModelMock DeserializeRecord(string value)
        {
            return (GridModelMock)TypeDescriptor.GetConverter(typeof(GridModelMock)).ConvertFromString(value);
        }

        public override string SerializeRecord(GridModelMock value)
        {
            return TypeDescriptor.GetConverter(typeof(GridModelMock)).ConvertToString(value);
        }
    }

    /// <summary>
    /// Server Side 
    /// </summary>
    public class ColorMapConfigMock : BaseConfiguration<DefaultConfiguration, ColorMapModelMock>
    {
        public ColorMapConfigMock(IConfigurationSettingsSource<DefaultConfiguration> source) : base(source)
        {
        }

        protected override ILogger GetLogger()
        {
            return null;
        }


        protected override string GetConfigurationName()
        {
            return nameof(ColorMapModelMock);
        }

        public override ColorMapModelMock DeserializeRecord(string value)
        {
            return (ColorMapModelMock)TypeDescriptor.GetConverter(GetType()).ConvertFromString(value);
        }

        public override string SerializeRecord(ColorMapModelMock value)
        {
            return TypeDescriptor.GetConverter(GetType()).ConvertToString(value);
        }
    }

    /// <summary>
    /// GridEntityMock  
    /// </summary>
    [TypeConverter(typeof(JsonTypeConverter<GridModelMock>))]
    public class GridModelMock
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// ColorMapEntityMock
    /// </summary>
    [TypeConverter(typeof(JsonTypeConverter<ColorMapModelMock>))]
    public class ColorMapModelMock
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class JsonTypeConverter<T> : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string stringValue = value as string;

            if (stringValue != null)
            {
                return JsonConvert.DeserializeObject<T>(value.ToString());
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;

            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                return JsonConvert.SerializeObject(value);
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

}
