﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bullet.Framework.Configuration.Entities.Parameters;
using Bullet.Framework.Configuration.Interfaces.Source;
using System.Linq;

namespace Bullet.Framework.Configuration.Test.Source
{
    public class SettingsSourceCacheMock : IConfigurationSettingsSource<DefaultConfiguration>
    {
        IList<DefaultConfiguration> sourceList = new List<DefaultConfiguration>();

        public SettingsSourceCacheMock()
        {
            sourceList.Add(new DefaultConfiguration { UserKey = 1, ConfigurationName = "GridConfig", ApplicationName = "BESTEVOLUTION", IsCached = true, Value = "{\"Name\":null,\"Key\":null,\"Value\":null}" });
            sourceList.Add(new DefaultConfiguration { UserKey = 2, ConfigurationName = "GridConfig", ApplicationName = "BESTEVOLUTION", IsCached = true, Value = "{\"Name\":null,\"Key\":null,\"Value\":null}" });
            sourceList.Add(new DefaultConfiguration { UserKey = 1, ConfigurationName = "ColorMapConfig", ApplicationName = "BESTEVOLUTION", IsCached = true, Value = "false" });
            sourceList.Add(new DefaultConfiguration { UserKey = 2, ConfigurationName = "ColorMapConfig", ApplicationName = "BESTEVOLUTION", IsCached = true, Value = "true" });
        }

        public async Task<object> LoadConfiguration(DefaultConfiguration genericParameters)
        {
            var data = sourceList.FirstOrDefault(t => t.ConfigurationName == genericParameters.ConfigurationName &&
                                                t.ApplicationName == genericParameters.ApplicationName &&
                                                t.UserKey == genericParameters.UserKey);

            return await new Task<object>(() => data.Value);
        }

        public Task<bool> SaveConfigurationAsync(DefaultConfiguration universalParameter, string value)
        {
            sourceList.Add(new DefaultConfiguration
            {
                UserKey = universalParameter.UserKey,
                ConfigurationName = universalParameter.ConfigurationName,
                ApplicationName = universalParameter.ApplicationName,
                IsCached = universalParameter.IsCached,
                Value = value
            });

            return Task.Run(() => true);
        }

    }
}
