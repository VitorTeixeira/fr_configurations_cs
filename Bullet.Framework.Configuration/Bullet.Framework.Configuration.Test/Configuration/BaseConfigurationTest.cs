﻿using System;
using Bullet.Framework.Configuration.Entities.Cache;
using Bullet.Framework.Configuration.Entities.Parameters;
using Bullet.Framework.Configuration.Test.Mock;
using Bullet.Framework.Configuration.Test.Source;
using NUnit.Framework;

namespace Bullet.Framework.Configuration.Test.Configuration
{
    [TestFixture]
    public class BaseConfigurationTest
    {
        //TODO : ver implementação de mock para source

        SettingsSourceCacheMock _source;
        
        [SetUp]
        public void InitSetUp()
        {
            _source = new SettingsSourceCacheMock();
        }

        [Test]
        public void InstantiateASettingsCacheWithAnInvalidSourceParameterShouldFail()
        {
            Assert.Throws<ArgumentNullException>(() => new SettingsCache<DefaultConfiguration>(null));
        }

        [Test]
        public void InstantiateAConfigurationWithAnInvalidSettingsSourceShouldFail()
        {
            Assert.Throws<ArgumentNullException>(() => new GridConfigMock(null));
        }

        [Test]
        public void RequestAGridConfigurationThatExistsShouldPass()
        {
            var parameterValue = new DefaultConfiguration()
            {
                ApplicationName = "BESTEVOLUTION",
                UserKey = 1,
                ConfigurationName = "GridConfig"
            };

            var gridConfiguration = new GridConfigMock(_source);
            var actualValue = gridConfiguration.GetValue(parameterValue);

            var expectedConfiguration = Guid.Parse("3f38edf7-24f9-4d80-9348-e2a3eb863a30");

            Assert.AreEqual(expectedConfiguration, actualValue);
        }

        [Test]
        public void SetARequestGridConfigurationShouldPass()
        {
            var parameterValue = new DefaultConfiguration()
            {
                ApplicationName = "BESTEVOLUTION",
                UserKey = 1,
                ConfigurationName = "GridConfig"
            };

            var gridConfiguration = new GridConfigMock(_source);
            var actualValue = gridConfiguration.SetValue(parameterValue, new GridModelMock());


            var gridConfiguration1 = new GridConfigMock(_source);
            var actualValue1 = gridConfiguration1.SetValue(parameterValue, new GridModelMock());
            
            var expectedConfiguration = Guid.Parse("3f38edf7-24f9-4d80-9348-e2a3eb863a30");

            Assert.AreEqual(true, actualValue);
        }
    }
}
